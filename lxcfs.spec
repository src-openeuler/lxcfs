#Global macro or variable
%define _unpackaged_files_terminate_build 0

#Basic Information
Name:      lxcfs
Version:   5.0.4
Release:   2
Summary:   FUSE filesystem for LXC
License:   LGPL 2.1+
URL:       http://linuxcontainers.org
Source:    https://linuxcontainers.org/downloads/%{name}/%{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-build
Patch9001: 0001-systemd.patch
Patch9002: 0002-show-dev-name-in-container.patch
Patch9003: 0003-lxcfs-fix-cpuinfo-print.patch
Patch9004: 0004-fix-memory-leak.patch
Patch9005: 0005-fix-concurrency-problem.patch
Patch9006: 0006-set-null-after-free.patch
Patch9007: 0007-limit-stat-by-quota-period-setting.patch
Patch9008: 0008-diskstats-support-devicemapper-device.patch
Patch9009: 0009-lxcfs-add-proc-partitions.patch
Patch9010: 0010-lxcfs-proc_diskstats_read-func-obtain-data-from-blki.patch
Patch9011: 0011-add-secure-compile-option-in-meson.patch
Patch9012: 0012-lxcfs-adapt-4.18-kernel.patch
Patch9013: 0013-enable-cfs-option-to-show-correct-proc-cpuinfo-view.patch
Patch9014: 0014-fix-pidfd_open-pidfd_send_signal-function-compilatio.patch
Patch9015: 0015-adapt-meson-build-install.patch
Patch9016: 0016-typofix-fix-incorrect-printing-in-lxcfs-help-interfa.patch

#Dependency
BuildRequires: meson python3-jinja2 help2man
BuildRequires: fuse-devel libattr-devel
BuildRequires: gcc systemd-units
Requires: fuse-libs

%description
lxcfs is a simple userspace filesystem designed to make containers
feel more like a real independent system through. It is usable by
any runtime, written in C using libfuse and glib.

%prep
%autosetup -p1

%build
make %{?_smp_mflags}

%install
install -d -m 0755 %{buildroot}%{_unitdir}
make install DESTDIR=%{buildroot} %{?_smp_mflags}
install -d -m 0755 %{buildroot}%{_sbindir}
ln -sf service %{buildroot}%{_sbindir}/rc%{name}
install -d -m 0755 %{buildroot}%{_includedir}/%{name}
install -d -m 0755 %{buildroot}%{_localstatedir}/lib/lxc/%{name}/

#Install and uninstall scripts
%pre

%preun
%systemd_preun lxcfs

%post
systemctl status lxcfs | grep 'Active:' | grep 'running'
if [ $? -eq 0 ]; then
	systemctl restart lxcfs
fi

%postun

#Files list
%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog README
%license COPYING
%{_sbindir}/*
%{_bindir}/*
%{_datadir}/lxc
%{_datadir}/%{name}
%dir %{_localstatedir}/lib/lxc/%{name}
%dir %{_libdir}/%{name}
%{_libdir}/%{name}/lib%{name}.so
%{_mandir}/man1/*
%{_unitdir}/*

%changelog
* Fri Dec 15 2023 yangjiaqi <yangjiaqi16@huawei.com> - 5.0.4-2
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:fix incorrect printing in lxcfs help interface

* Thu July 27 2023 yangjiaqi <yangjiaqi16@huawei.com> - 5.0.4-1
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:upgrade to 5.0.4

* Mon July 17 2023 yangjiaqi <yangjiaqi16@huawei.com> - 5.0.3-1
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:upgrade to 5.0.3

* Wed Mar 22 2023 yangjiaqi <yangjiaqi16@huawei.com> - 4.0.11-8
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:fix /proc/diskstats output format

* Wed Mar 22 2023 yangjiaqi <yangjiaqi16@huawei.com> - 4.0.11-7
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:fix possible use-after-free in find_proc_stat_node

* Mon Dec 12 2022 yangjiaqi <yangjiaqi16@huawei.com> - 4.0.11-6
- add yaml

* Thu Nov 24 2022 yangjiaqi <yangjiaqi16@huawei.com> - 4.0.11-5
- fix pidfd_open&pidfd_send_signal function compilation bug in glibc2.36+

* Mon Jul 04 2022 yangjiaqi <yangjiaqi16@huawei.com> - 4.0.11-4
- enable cfs option to show correct proc cpuinfo view

* Wed May 11 2022 zhangsong234 <zhangsong34@huawei.com> - 4.0.11-3
- fix dev read memory leak in container

* Fri Dec 17 2021 yangjiaqi <yangjiaqi16@huawei.com> - 4.0.11-2
- fix deadlock caused by subprocess calling lxcfs_exit

* Sat Nov 27 2021 yangjiaqi <yangjiaqi16@huawei.com> - 4.0.11-1
- update to 4.0.11

* Mon Jun 28 2021 zhangsong234 <zhangsong34@huawei.com> - 4.0.5-6
- fix rpm build failed

* Thu May 27 2021 zhangsong234 <zhangsong34@huawei.com> - 4.0.5-5
- proc_fuse: fix wait child process hang

* Tue Mar 30 2021 zhangsong234 <zhangsong34@huawei.com> - 4.0.5-4
- remove lxcfs-tools dependency for common use

* Mon Nov 30 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.0.5-2 
- fix reinstall bug

* Fri Aug 28 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.0.5-1 
- update to 4.0.5

* Thu Jul 02 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.0.2-0.12
- Sync from internal changes

* Thu Dec 26 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.0.2-0.5.h2
- Remove securec functions

* Wed Sep 18 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.0.2-0.5.h1
- Package init
